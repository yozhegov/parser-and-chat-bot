﻿
using Day011_SkillBox_Homework.Infrastructure;
using Day011_SkillBox_Homework.Reports;
using Day011_SkillBox_Homework.Storages;
using Ninject;
using Ninject.Modules;

namespace Day011_SkillBox_Homework
{
    class Program
    {
        static void Main()
        {
            new StandardKernel(new Bindings()).Get<Application>().Run();
        }
    }

    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IMeasureStorage>().To<XmlFileMeasureStorage>().WithConstructorArgument(@"path", "./homework.xml");
            Bind<IOutputProvider>().To<XmlFileOutputProvider>().WithConstructorArgument(@"path", "./homework_answer.xml");
            Bind<IReportBuilder>().To<ReportBuilder>();
        }
    }
}
