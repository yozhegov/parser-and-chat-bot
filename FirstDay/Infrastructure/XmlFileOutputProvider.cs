﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Day011_SkillBox_Homework.Reports.Models;

namespace Day011_SkillBox_Homework.Infrastructure
{
    public class XmlFileOutputProvider : IOutputProvider
    {
        private readonly string path;

        public XmlFileOutputProvider(string path)
        {
            this.path = path;
        }

        public void OutPutInfo(ReportData reportData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ReportData));
            using (var fs = new FileStream(path, FileMode.Create))
            {
                using (var writer = new XmlTextWriter(fs, Encoding.Unicode))
                {
                    serializer.Serialize(writer, reportData);
                }
            }
        }
    }
}
