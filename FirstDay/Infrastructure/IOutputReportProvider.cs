﻿using Day011_SkillBox_Homework.Reports.Models;

namespace Day011_SkillBox_Homework.Infrastructure
{
    public interface IOutputProvider
    {
        void OutPutInfo(ReportData reportData);
    }
}
