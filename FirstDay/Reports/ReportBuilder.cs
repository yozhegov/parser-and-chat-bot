﻿using System.Linq;
using Day011_SkillBox_Homework.DomainModels;
using Day011_SkillBox_Homework.Reports.Models;

namespace Day011_SkillBox_Homework.Reports
{
    public class ReportBuilder : IReportBuilder
    {
        public ReportData Build(Measure[] measures)
        {
            var temperaturesReportData = measures
                .GroupBy(x => x.Temperature)
                .Select(g => new TemperatureReportData { Temperature = g.Key, Count = g.Count() })
                .ToArray();
            return new ReportData
            {
                TemperatureReportData = temperaturesReportData
            };
        }
    }
}
