﻿using System.Xml.Serialization;

namespace Day011_SkillBox_Homework.Reports.Models
{
    public class TemperatureReportData
    {
        [XmlElement("Temperature")]
        public int Temperature { get; set; }
        [XmlElement("Count")]
        public int Count { get; set; }
    }
}
