﻿using System;
using System.Xml.Serialization;

namespace Day011_SkillBox_Homework.Reports.Models
{
    [Serializable()]
    [XmlRoot("Data")]
    public class ReportData
    {
        [XmlElement("City", typeof(TemperatureReportData))]
        public TemperatureReportData[] TemperatureReportData { get; set; }
    }
}
