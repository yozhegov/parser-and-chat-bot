﻿using Day011_SkillBox_Homework.DomainModels;
using Day011_SkillBox_Homework.Reports.Models;

namespace Day011_SkillBox_Homework.Reports
{
    public interface IReportBuilder
    {
        ReportData Build(Measure[] measures);
    }
}
