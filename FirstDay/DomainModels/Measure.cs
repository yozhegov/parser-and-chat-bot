﻿using System;

namespace Day011_SkillBox_Homework.DomainModels
{
    public struct Measure
    {
        public Measure(
            DateTime measureDate,
            string cityName,
            int pressure,
            int temperature,
            int relWet
            )
        {
            MeasureDate = measureDate;
            CityName = cityName;
            Pressure = pressure;
            Temperature = temperature;
            RelWet = relWet;
        }
        public DateTime MeasureDate { get; }
        public string CityName { get; }
        public int Pressure { get; }
        public int Temperature { get; }
        public int RelWet { get; }
    }
}
