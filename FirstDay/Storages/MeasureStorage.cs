﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Day011_SkillBox_Homework.DomainModels;
using Day011_SkillBox_Homework.Storages.DataModels;

namespace Day011_SkillBox_Homework.Storages
{
    public class XmlFileMeasureStorage : IMeasureStorage
    {
        private readonly MeasureDbo[] measures;
        public XmlFileMeasureStorage(string path)
        {
            var xmlSource = File.ReadAllText(path);
            using (var reader = new StringReader(xmlSource))
            {
                var report = (ReportDbo)new XmlSerializer(typeof(ReportDbo)).Deserialize(reader);
                measures = report.Measures;
            }
        }
        public Measure[] SelectAll()
        {
            return measures
                .Select(Convert)
                .ToArray();
        }

        private static Measure Convert(MeasureDbo measureDbo)
        {
            return new Measure(
                new System.DateTime(measureDbo.Year, measureDbo.Month, measureDbo.Day),
                measureDbo.CityName,
                measureDbo.Pressure,
                measureDbo.Temperature.Value,
                measureDbo.RelWet
            );
        }
    }
}
