﻿using Day011_SkillBox_Homework.DomainModels;

namespace Day011_SkillBox_Homework.Storages
{
    public interface IMeasureStorage
    {
        Measure[] SelectAll();
    }
}
