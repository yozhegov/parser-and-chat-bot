﻿using System;
using System.Xml.Serialization;

namespace Day011_SkillBox_Homework.Storages.DataModels
{
    [Serializable()]
    [XmlRoot("WEATHER")]
    public class ReportDbo
    {
        [XmlArray("REPORT")]
        [XmlArrayItem("TOWN", typeof(MeasureDbo))]
        public MeasureDbo[] Measures { get; set; }
    }
}
