﻿using System;
using System.Xml.Serialization;

namespace Day011_SkillBox_Homework.Storages.DataModels
{
    [Serializable()]
    public class MeasureDbo
    {
        [XmlAttribute("day")]
        public int Day { get; set; }
        [XmlAttribute("month")]
        public int Month { get; set; }
        [XmlAttribute("year")]
        public int Year { get; set; }

        [XmlElement("CITYNAME")]
        public string CityName { get; set; }
        [XmlElement("PRESSURE")]
        public int Pressure { get; set; }
        [XmlElement("TEMPERATURE")]
        public TemperatureDbo Temperature { get; set; }
        [XmlElement("RELWET")]
        public int RelWet { get; set; }
    }

    [Serializable()]
    public class TemperatureDbo
    {
        [XmlAttribute("value")]
        public int Value { get; set; }
    }
}
