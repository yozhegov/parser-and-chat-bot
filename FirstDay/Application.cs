using Day011_SkillBox_Homework.Infrastructure;
using Day011_SkillBox_Homework.Reports;
using Day011_SkillBox_Homework.Storages;

namespace Day011_SkillBox_Homework
{
    public class Application
    {
        private readonly IMeasureStorage measureStorage;
        private readonly IReportBuilder reportBuilder;
        private readonly IOutputProvider outputProvider;

        public Application(
            IMeasureStorage measureStorage,
            IReportBuilder reportBuilder,
            IOutputProvider outputProvider
        )
        {
            this.measureStorage = measureStorage;
            this.reportBuilder = reportBuilder;
            this.outputProvider = outputProvider;
        }

        public void Run()
        {
            var measures = measureStorage.SelectAll();
            var report = reportBuilder.Build(measures);
            outputProvider.OutPutInfo(report);
        }
    }
}